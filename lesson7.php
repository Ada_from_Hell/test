<?php
function dd()
{
    echo "<pre>";
    print_r($arr);
    echo "</pre>";
}

// 1. Найти минимальное и максимальное среди 3 чисел.
$a = rand(1, 100);
$b = rand(1, 100);
$c = rand(1, 100);

// 1. 1. function expression
$checkMinAndMax = function ($a, $b, $c) {
    return 'Минимальное значение: ' . min($a, $b, $c) . "</br>" . 'Максимальное значение: ' . max($a, $b, $c);
};
echo $checkMinAndMax($a, $b, $c);

// 1. 2. Стрелочная функция
$checkMinAndMax = fn($a, $b, $c) => 'Минимальное значение: ' . min($a, $b, $c) . "</br>" . 'Максимальное значение: ' . max($a, $b, $c);
echo $checkMinAndMax($a, $b, $c);

// 2. Найти площадь
$a = rand(1, 100);
$b = rand(1, 100);
// 2. 1. function expression
$calcSquare = function ($a, $b) {
    return $a * $b;
};
echo $calcSquare($a, $b);
// 2. 2. Стрелочная функция
$calcSquare = fn($a, $b) => $a * $b;
echo $calcSquare($a, $b);

// 3. Теорема Пифагора
$a = rand(1, 100);
$b = rand(1, 100);
$aa = $a * $a;
$bb = $b * $b;
$cc = $aa + $bb;

// 3. 1. function expression
$calcPythagoreantheorem = function ($aa, $bb, $cc) {
    return $aa + $bb + $cc;
};
echo $calcPythagoreantheorem($aa, $bb, $cc);

// 3. 2. Стрелочная функция
$calcPythagoreantheorem = fn($aa, $bb, $cc) => $aa + $bb + $cc;
echo $calcPythagoreantheorem($aa, $bb, $cc);

// 4. Найти периметр
$a = rand(1, 100);
$b = rand(1, 100);

// 4. 1. function expression
$calcPerimeter = function ($a, $b) {
    return 2 * ($a + $b);
};
echo $calcPerimeter($a, $b);

// 4. 2. Стрелочная функция
$calcPerimeter = fn($a, $b) => 2 * ($a + $b);
echo $calcPerimeter($a, $b);

// 5. Найти дискриминант
$a = rand(1, 100);
$b = rand(1, 100);
$c = rand(1, 100);

// 5. 1. function expression
$calcDiscriminant = function ($a, $b, $c) {
    return ($b * $b - (4 * $a * $c));
};
echo $calcDiscriminant($a, $b, $c);

// 5. 2. Стрелочная функция
$calcDiscriminant = fn($a, $b, $c) => ($b * $b - (4 * $a * $c));
echo $calcDiscriminant($a, $b, $c);

// 6. Создать только четные числа до 100
function dd($arr)
{
    echo "<pre>";
    print_r($arr);
    echo "</pre>";
}

$arr = range(1, 100);

$result = array_filter($arr, function ($val) {
    if ($val % 2 == 0) {
        return $val;
    } else {
        return false;
    }
}
);

function arrayFilter($arr, $fn)
{
    $result = [];
    $count = count($arr);
    for ($i = 0; $i < $count; $i++) {
        if (!empty($fn($arr[$i]))) {
            $result [] = $arr[$i];
        }
    }
    return $result;
}

// 6. 1. function expression
$ttt = arrayFilter($arr, function ($item) {
    if ($item % 2 == 0) {
        return $item;
    }
});

// 6. 2. Стрелочная функция
$ttt = arrayFilter($arr, fn($item) => $item % 2 == 0 ? $item : false);
dd($ttt);

// 7. Создать нечетные числа до 100
function dd($arr)
{
    echo "<pre>";
    print_r($arr);
    echo "</pre>";
}

$arr = range(1, 100);
$result = array_filter($arr, function ($val) {
    if ($val % 2 == 1) {
        return $val;
    } else {
        return false;
    }
}
);
function arrayFilter($arr, $fn)
{
    $result = [];
    $count = count($arr);
    for ($i = 0; $i < $count; $i++) {
        if (!empty($fn($arr[$i]))) {
            $result [] = $arr[$i];
        }
    }
    return $result;
}

// 7. 1. function expression
$ttt = arrayFilter($arr, function ($item) {
    if ($item % 2 == 1) {
        return $item;
    }
});

// 7. 2. Стрелочная функция
$ttt = arrayFilter($arr, fn($item) => $item % 2 == 1 ? $item : false);
dd($ttt);

// 8. Определите, есть ли в массиве повторяющиеся элементы.
$arr = [1 => '1', 2 => '2', 3 => '3', 4 => '4', 5 => '5'];

// 8. 1. function expression
$repeatingElements = function ($arr) {
    return array_unique($arr);
};
print_r($repeatingElements($arr));

// 8. 2. Стрелочная функция
$repeatingElements = fn($arr) => (array_unique($arr));
print_r($repeatingElements($arr));

// 9. Сортировка из прошлого задания
function dd($result)
{
    echo "<pre>";
    print_r($result);
    echo "</pre>";
}

$arr = [1 => '1', 2 => '2', 3 => '3', 4 => '4', 5 => '5'];
$result = array_map(fn($val) => $val * 2, $arr);
dd($result);

// 8. 1. function expression
$res = function ($callback, $arr) {
    $count = count($arr);
    $result = [];
    for ($i = 0; $i < $count; $i++) {
        $result [] = $callback($arr[$i]);
    }
    return $result;
};

// 8. 2. Стрелочная функция
$result = array_map(fn($val) => $val * 2, $arr);
dd($result);