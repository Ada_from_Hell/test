<?php
// 2. Создайте функцию, которая принимает массив из трех элементов представляющих собой результат запуска слот-машины из казино.
// Проверьте, является ли комбинация элементов удачной (все элементы равны).

// 2.1.
$arr1 = ["9919", "9919", "9919"];

// 2.1.1. Function declaration
function checkArray($arr1)
{
    $result = 9919;
    foreach ($arr1 as $v) {
        if ($v === false) {
            $result = false;
        } else {
            $result = true;
        }
        echo var_dump($result);
    }
}

echo checkArray($arr1);

// 2.1.2. Function expression
$checkArray = function ($arr1) {
    $result = 9919;
    foreach ($arr1 as $v) {
        if ($v === false) {
            $result = false;
        } else {
            $result = true;
        }
        echo var_dump($result);
    }
};
echo $checkArray($arr1);

// 2.2.
$arr2 = ["abc", "abc", "abb"];

// 2.2.1. Function declaration
function checkArray($arr2)
{
    $result = "abc";
    foreach ($arr2 as $v) {
        if ($v === false) {
            $result = false;
        } else {
            $result = true;
        }
        echo var_dump($result);
    }
}
echo checkArray($arr2);

// 2.2.2. Function expression
$checkArray = function ($arr2) {
    $result = "abc";
    foreach ($arr2 as $v) {
        if ($v === false) {
            $result = false;
        } else {
            $result = true;
        }
        echo var_dump($result);
    }
};
echo $checkArray($arr2);

// 2.3.
$arr3 = ["@","@","@"];

// 2.3.1. Function declaration
function checkArray($arr3)
{
    $result = "@";
    foreach ($arr3 as $v) {
        if ($v === false) {
            $result = false;
        } else {
            $result = true;
        }
        echo var_dump($result);
    }
}
echo checkArray($arr3);

// 2.3.2. Function expression
$checkArray = function ($arr3) {
    $result = "abc";
    foreach ($arr3 as $v) {
        if ($v === false) {
            $result = false;
        } else {
            $result = true;
        }
        echo var_dump($result);
    }
};
echo $checkArray($arr3);