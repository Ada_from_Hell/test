<?php
//	1. Дан массив ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']. Развернуть этот массив в обратном направлении
	$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
    $count = 0;
    while (!empty($arr[$count])) {
        $count++;
    }

// 	1.1. while
    $i = 0;
    while ($i < $count) {
        $j = 0;
        while ($j < $count-1) {
            if($arr[$j] < $arr[$j+1]) {
                $temp = $arr[$j+1];
                $arr[$j+1] = $arr[$j];
                $arr[$j] = $temp;
            }
            $j++;
        }
        $i++;
    }
    print_r($arr);

// 1.2. do while
    $i = $count - 1;
    do {
        $res[] = $arr[$i];
    }
    while ($i >= 0 && $i--);
    print_r($res);

//	1.3. for
	$res = [];
	for ($i = $count - 1; $i >= 0; $i--) {
	   $res[] = $arr[$i];
	}
	print_r($res);

//	1.4. foreach
    for ($i = $count - 1; $i >= 0; $i--) {
        $res[] = $arr[$i];
    }
    foreach($res as $v)
        echo $v.'</br>';

//	2. Дан массив [44, 12, 11, 7, 1, 99, 43, 5, 69]. Развернуть этот массив в обратном направлении.
	$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
	$count = 0;
	foreach($arr as $val) {
	   $count++;
	}
	$res = [];
	$i = $count;

// 2.1. while
	while ($i >= 0 && $i--) {
	   $res[] = $arr[$i];
	}
	print_r($res);

// 2.2. do while
    $i = $count - 1;
    do {
        $res[] = $arr[$i];
    }
    while ($i >= 0 && $i--);
    print_r($res);

// 2.3. for
    $res = [];
    for ($i = $count - 1; $i >= 0; $i--) {
        $res[] = $arr[$i];
    }
    print_r($res);

// 2.4. foreach
    for ($i = $count - 1; $i >= 0; $i--) {
        $res[] = $arr[$i];
    }
    foreach($res as $v)
        echo $v.'</br>';

//  3. Дана строка let str = 'Hi I am ALex'. Развенуть строку в обратном направлении.

    $letStr = 'Hi I am ALex';
    $letStrRevers = '';

// 3.1. while
    $i = 0;
    while (!empty($letStr[$i])) {
        $letStrRevers = $letStr[$i].$letStrRevers;
        $i++;
    }
    echo $letStrRevers;

// 3.2. do while
    $i = 0;
    do {
        $letStrRevers = $letStr[$i].$letStrRevers;
        $i++;
    }
    while (!empty($letStr[$i]));
    echo $letStrRevers;

// 3.3. for
    for ($i = 0; !empty($letStr[$i]); $i++) {
        $letStrRevers = $letStr[$i].$letStrRevers;
    }
    echo $letStrRevers;

// 3.4. foreach
    for ($i = 0; !empty($letStr[$i]); $i--) {
        $arr[] = $letStr[$i].$letStrRevers;
    }
    foreach($arr as $v)
        echo $v;

// 4. Дана строка. готовую функцию toUpperCase() or tolowercase() let str = 'Hi I am ALex' сделать ее с с маленьких букв
    $letStr = 'Hi I am ALex';
    $letStrToLowerCase = strtr ($letStr, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz');

// 4.1. while
    $i = 0;
    while ($i <= 0) {
        echo $letStrToLowerCase;
        $i++;
    }

// 4.2. do while
    $i = 0;
    do {
        echo $letStrToLowerCase;
        $i++;
    }
    while ($i <= 0);

// 4.3. for
    for ($i = 0; $i <= 0; $i++) {
        echo $letStrToLowerCase;
    }

// 4.4. foreach
    for ($i = 0; $i <= 0; $i++) {
        $arr[] = $letStrToLowerCase;
    }
    foreach($arr as $v)
        echo $v;

// 5. Дана строка let str = 'Hi I am ALex' сделать все буквы большие

    $letStr = 'Hi I am ALex';
    $letStrToUpperCase = strtr ($letStr, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ');

// 5.1. while
    $i = 0;
    while ($i <= 0) {
        echo $letStrToUpperCase;
        $i++;
    }

// 5.2. do while
    $i = 0;
    do {
        echo $letStrToUpperCase;
        $i++;
    }
    while ($i <= 0);

// 5.3. for
    for ($i = 0; $i <= 0; $i++) {
        echo $letStrToUpperCase;
    }

// 5.4. foreach
    for ($i = 0; $i <= 0; $i++) {
        $arr[] = $letStrToUpperCase;
    }
    foreach($arr as $v)
        echo $v;

//  7. Дан массив ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'] сделать все буквы с маленькой
    $arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
    $search = ["A", "B","C", "D","E","F","G","H", "I","J","K","L","M","N","O","P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
    $replace = ["a", "b","c", "d","e","f","g","h", "i","j","k","l","m","n","o","p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
    $arrSmall = str_replace($search, $replace, $arr);

// 7.1. while
    $i = 0;
    while ($i <= 0) {
        "<pre>";
        print_r($arrSmall);
        "</pre>";
        $i++;
    }

// 7.2. do while
    $i = 0;
    do {
        "<pre>";
        print_r($arrSmall);
        "</pre>";
        $i++;
    }
    while ($i <= 0);

// 7.3. for
    for($i = 0; $i < $count; $i++) {
        for($j = 0; $j < $count-1; $j++){
            if($arr[$j] < $arr[$j+1]) {
                $temp = $arr[$j+1];
                $arr[$j+1]=$arr[$j];
                $arr[$j]=$temp;
            }
        }
    }
    print_r($arr);

// 7.4. foreach
    for ($i = 0; $i <= 0; $i++) {
        $arr[] = $arrSmall;
    }
    foreach($arr as $v)
    "<pre>";
    print_r($v);
    "</pre>";

// 8. Дан массив ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'] сделать все буквы с большой
    $arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
    $search = ["a", "b","c", "d","e","f","g","h", "i","j","k","l","m","n","o","p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
    $replace = ["A", "B","C", "D","E","F","G","H", "I","J","K","L","M","N","O","P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];

// 8.1. while
    $i = 0;
    while ($i <= 0) {
    "<pre>";
    print_r($arrSmall);
    "</pre>";
    $i++;
}

// 8.2. do while
    $i = 0;
    do {
        "<pre>";
        print_r($arrSmall);
        "</pre>";
        $i++;
    }
    while ($i <= 0);

// 8.3. for
    for ($i = 0; $i <= 0; $i++) {
        "<pre>";
        print_r($arrSmall);
        "</pre>";
        $i++;
}

// 8.4. foreach
    for ($i = 0; $i <= 0; $i++) {
        $arr[] = $arrSmall;
    }
    foreach($arr as $v)
        "<pre>";
    print_r($v);
    "</pre>";

//  9. Дано число let num = 1234678 развернуть ее в обратном направлении
    $letNum = '1234678';
    $letNumRevers = '';

// 9.1. while
    $i = 0;
    while (!empty($letNum[$i])) {
        $letNumRevers = $letNum[$i].$letNumRevers;
        $i++;
    }
    echo $letNumRevers;

// 9.2 do while
    $i = 0;
    do {
        $letNumRevers = $letNum[$i].$letNumRevers;
        $i++;
    }
    while (!empty($letNum[$i]));
    echo $letNumRevers;

// 9.3 for
    for ($i = 0; !empty($letNum[$i]); $i++) {
        $letNumRevers = $letNum[$i].$letNumRevers;
    }
    echo $letNumRevers;

// 9.4 foreach
    for ($i = 0; !empty($letNum[$i]); $i--) {
        $arr[] = $letNum[$i].$letNumRevers;
    }
    foreach($arr as $v)
        echo $v;

// 10. Дан массив [44, 12, 11, 7, 1, 99, 43, 5, 69] отсортируй его в порядке убывания
    $arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
    $count = 0;
    while (!empty($arr[$count])) {
        $count++;
    }

// 	10.1. while
    $i = 0;
    while ($i < $count) {
        $j = 0;
        while ($j < $count-1) {
            if($arr[$j] < $arr[$j+1]) {
                $temp = $arr[$j+1];
                $arr[$j+1] = $arr[$j];
                $arr[$j] = $temp;
            }
            $j++;
        }
        $i++;
    }
    print_r($arr);

// 	10.2. do while
    $i = 0;
    do {
        $j = 0;
        while ($j < $count-1) {
            if($arr[$j] < $arr[$j+1]) {
                $temp = $arr[$j+1];
                $arr[$j+1] = $arr[$j];
                $arr[$j] = $temp;
            }
            $j++;
        }
        $i++;
    }
    while ($i < $count);
    print_r($arr);

//	10.3. for
    for($i = 0; $i < $count; $i++) {
        for($j = 0; $j < $count-1; $j++){
            if($arr[$j] < $arr[$j+1]) {
                $temp = $arr[$j+1];
                $arr[$j+1]=$arr[$j];
                $arr[$j]=$temp;
            }
        }
    }
    print_r($arr);

//	10.4. for
    for($i = 0; $i < $count; $i++) {
        for($j = 0; $j < $count-1; $j++){
            if($arr[$j] < $arr[$j+1]) {
                $temp = $arr[$j+1];
                $arr[$j+1]=$arr[$j];
                $arr[$j]=$temp;
            }
        }
    }
    foreach($res as $v)
        echo $v.'</br>';