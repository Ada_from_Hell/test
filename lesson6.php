<?php
// 1. Вам нужно создать массив и заполнить его случайными числами от 1 до 100 (ф-я rand). 
//Далее, вычислить произведение тех элементов, которые больше нуля и у которых четные индексы. 
//После вывести на экран элементы, которые больше нуля и у которых нечетный индекс.
    $arr = [];
    for ($i = 0; $i < 10; $i++) {

    }
    echo '<pre>';
    print_r($arr);
    echo '</pre>';

// 1.1. Произведение элементов с чётными индексами, которые больше нуля
    function calcProductHonestArr($arr) {
        $productHonestArr = $arr[0];
        for ($i = 2; $i < count($arr); $i += 2) {
            if ($arr[$i] > 0) {
                $productHonestArr *= $arr[$i];
            }
        }
        echo 'Произведение элементов с чётными индексами, которые больше нуля: '. $productHonestArr. "</br>";
        return true;
    };
    echo calcProductHonestArr($arr);

// 1.2. Элементы с нечётными индексами, которые больше нуля
    function calcProductOddArr($arr) {
        echo 'Элементы с нечётными индексами, которые больше нуля.'. "</br>";
        foreach ($arr as $key => $value) {
            if ($value > 0 && $key % 2) {
                echo $value. "</br>";
            }
        }
        return true;
    }
    echo calcProductOddArr($arr);

// 2. Даны два числа. Найти их сумму и произведение. Даны два числа. Найдите сумму их квадратов.
// 2.1.1. Сумма
	$a = rand(1, 100);
    $b = rand(1, 100);
    function calcSum($a, $b) {
        return $a + $b;
    }
    echo calcSum($a, $b));

// 2.1.2. Произведение
	$a = rand(1, 100);
    $b = rand(1, 100);
    function calcProduct($a, $b) {
        return $a * $b;
    }
    echo calcProduct($a, $b);

// 2.1.3. Сумма квадратов
	$a = rand(1, 100);
    $b = rand(1, 100);
    function calcSumSquare($a, $b) {
        return ($a * $a) + ($b * $b);
    }
    echo calcSumSquare($a, $b);
	
// 3. Даны три числа. Найдите их среднее арифметическое.
    $a = rand(1, 100);
    $b = rand(1, 100);
    $с = rand(1, 100);
	function calcAverage($a, $b, $c) {
        return ($a + $b + $c) / 3;
    }
	echo calcAverage($a, $b, $c);

// 4. Дано число. Увеличьте его на 30%, на 120%.
    $a = rand (1, 100);
// 4.1. 30%
    function calcIncrease30($a) {
        return ($a += ($a * 0.3));
    };
    echo calcIncrease30($a);

// 4.2. 120%
    function calcIncrease120($a) {
        return ($a += ($a * 1.2));
    };
    echo calcIncrease120($a);

// 5. Пользователь выбирает из выпадающего списка страну (Турция, Египет или Италия), 
// вводит количество дней для отдыха и указывает, есть ли у него скидка (чекбокс). 
// Вывести стоимость отдыха, которая вычисляется как произведение количества дней на 400. 
// Далее это число увеличивается на 10%, если выбран Египет, и на 12%, если выбрана Италия. 
// И далее это число уменьшается на 5%, если указана скидка.
    function calcPrice() {
        $country = 'Италия';
        $days = 4;
        $sale = 'yes';
        $price = $days * 400;
        $priceTurkey = $price;
        $priceEgypt = $price + ($price * 0.1);
        $priceItaly = $price + ($price * 0.12);
        $priceSaleTurkey = $priceTurkey - ($priceItaly * 0.05);
        $priceSaleEgypt = $priceEgypt - ($priceEgypt * 0.05);
        $priceSaleItaly = $priceItaly - ($priceItaly * 0.05);
        if ($country == 'Турция') {
            echo $priceTurkey;
        }
        elseif ($country == 'Египет') {
            echo $priceEgypt;
        }
        elseif ($country == 'Италия') {
            echo $priceItaly;
        }
        elseif ($country == 'Турция' && $sale == 'yes') {
            echo $priceSaleTurkey;
        }
        elseif ($country == 'Турция' && $sale == 'yes') {
            echo $priceSaleEgypt;
        }
        elseif ($country == 'Италия' && $sale == 'yes') {
            echo $priceSaleItaly;
        }
        else {
            echo 'Выберите, пожаоуйста, страну из списка и укажите количество дней, а также наличие скидки.';
        }
        return true;
    }
    echo calcPrice();

//  6. Пользователь вводит свой имя, пароль, email.
// Если вся информация указана, то показать эти данные после фразы 'Регистрация прошла успешно',
// иначе сообщить какое из полей оказалось не заполненным.
	$login = 'd';
    $password = 's';
    $email = 's';
    function getRegistration($login, $password, $email) {
        
        if (!empty($login) && !empty($password) && !empty($email)) {
            echo 'Регистрация прошла успешно!';
        }
        elseif (empty($login)) {
            echo 'Введите, пожалуйста, Ваше имя.'. "</br>";
        }
        elseif (empty($password)) {
            echo 'Придумайте пароль.'. "</br>";
        }
        elseif (empty($email)) {
            echo 'Введите адрес электронной почты.'. "</br>";
        }
        else {
            echo 'Заполните, пожалуйста, все необходимые поля.';
        }
        return true;
    }
	echo getRegistration ($login, $password, $email);

//7. Выведите на экран n раз фразу "Silence is golden".
// Число n вводит пользователь на форме.
// Если n некорректно, вывести фразу "Bad n".
	$i = 0;
    function showMessage($i) {
        if ($i == 0) {
            for ($i = 0; $i < 10; $i++) {
                echo 'Silence is golden'. "</br>";
            }
        }
        else {
            echo 'Bad n'. "</br>";
        }
        return true;
    }
    echo showMessage($i);

// 8. Заполнить массив длины n нулями и единицами, при этом данные значения чередуются, начиная с нуля.
    $i = 0;
	function createArr($i) {
        for ($i = 0; $i < 10; $i++) {
            $arr[] = ($i%2 == 0) ? 1 : 0;
        }
        print_r($arr);
        return true;
    }
    echo createArr($i);

// 9. Определите, есть ли в массиве повторяющиеся элементы.
    $arr = [1 => 'one', 2 => 'two', 3 => 'three', 4 => 'fourth', 5 => 'five'];
	function checkRepeatingElements($arr) {
        $repeatingElements = array_unique($arr);
        print_r($repeatingElements);
        return true;
    }
    echo checkRepeatingElements($arr);

// 10. Найти минимальное и максимальное среди 3 чисел.
	$a = rand(1, 100);
    $b = rand(1, 100);
    $c = rand(1, 100);
    function checkMinAndMax($a, $b, $c) {
        echo 'Минимальное значение: '. min ($a, $b, $c). "</br>";
        echo 'Максимальное значение: '. max ($a, $b, $c). "</br>";
        return true;
    }
    echo checkMinAndMax($a, $b, $c);

// 11. Найти площадь
	$a = rand(1, 100);
    $b = rand(1, 100);
    function calcSquare($a, $b) {
        return $a * $b;
    }
    echo calcSquare($a, $b);

// 12. Теорема Пифагора
	$a = rand(1, 100);
    $b = rand(1, 100);
	$aa = $a * $a;
    $bb = $b * $b;
    $cc = $aa + $bb;
    function calcPythagoreantheorem($aa, $bb, $cc) {
        return $aa + $bb + $cc;
    }
    echo calcPythagoreantheorem($aa, $bb, $cc);

// 13. Найти периметр
	$a = rand(1, 100);
    $b = rand(1, 100);
    function calcPerimeter($a, $b) {
        return 2 * ($a + $b);
    }
    echo calcPerimeter($a, $b);

// 14. Найти дискриминант
	$a = rand(1, 100);
    $b = rand(1, 100);
    $c = rand(1, 100);
    function calcDiscriminant($a, $b, $c) {
        return ($b * $b - (4 * $a * $c));
    }
    echo calcDiscriminant($a, $b, $c);

// 15. Создать только четные числа до 100
    $arr = [];
	function createEvenNumbersUpTo100($arr) {
        for ($x = 1; $x <= 100; $x++) {
            if ($x % 2 == 0) {
                echo $x. "</br>";
            }
        }
		return true;
    }
    echo createEvenNumbersUpTo100($arr);

// 16. Создать не четные числа до 100
	$i = 0;
    function createOddNumbersUpTo100($i) {
        for ($i = 1; $i <= 100; $i++) {
            if ($x % 2 == 1) {
                echo $i. "</br>";
            }
        }
		return true;
    }
    echo createOddNumbersUpTo100($i);
?>