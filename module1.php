<?php
// 1. Создайте функцию, которая подсчитывает количество директорий в массиве.
//1.1.
$arr1 = ["C:/Projects/something.txt", "file.exe"];

// 1.1.1. Function declaration
function calcDir($arr1) {
    for ($i = 0; $i <= 0; $i++) {
        if (in_array("/", $arr1)) {
            return "Не нашёл директорий. " . "</br>";
        } else {
            return "Нашёл директорий. " . "</br>";
        }
    }
}
echo calcDir($arr1);

// 1.1.2. Function expression
$calcDir = function($arr1) {
    for ($i = 0; $i <= 0; $i++) {
        if (in_array("/", $arr1)) {
            return "Не нашёл директорий. " . "</br>";
        } else {
            return "Нашёл директорий. " . "</br>";
        }
    }
};
echo $calcDir($arr1);

// 1.2.
$arr2 = ["brain-games.exe","gendiff.sh","task-manager.rb"];

// 1.2.1. Function declaration
function calcDir($arr2) {
    for ($i = 0; $i <= 0; $i++) {
        if (in_array("/", $arr2)) {
            return "Не нашёл директорий. " . "</br>";
        } else {
            return "Нашёл директорий. " . "</br>";
        }
    }
}
echo calcDir($arr2);

// 1.2.2. Function expression
$calcDir = function($arr2) {
    for ($i = 0; $i <= 0; $i++) {
        if (in_array("/", $arr2)) {
            return "Не нашёл директорий. " . "</br>";
        } else {
            return "Нашёл директорий. " . "</br>";
        }
    }
};
echo $calcDir($arr2);

// 1.3.
$arr3 = ["C:/Users/JohnDoe/Music/Beethoven_5.mp3","/usr/bin","/var/www/myprojectt"];

// 1.3.1. Function declaration
function calcDir($arr3) {
    for ($i = 0; $i <= 0; $i++) {
        if (in_array("/", $arr3)) {
            return "Не нашёл директорий. " . "</br>";
        } else {
            return "Нашёл директорий. " . "</br>";
        }
    }
}
echo calcDir($arr3);

// 1.3.2. Function expression
$calcDir = function($arr3) {
    for ($i = 0; $i <= 0; $i++) {
        if (in_array("/", $arr3)) {
            return "Не нашёл директорий. " . "</br>";
        } else {
            return "Нашёл директорий. " . "</br>";
        }
    }
};
echo $calcDir($arr3);