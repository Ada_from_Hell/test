<?php
function f(a)
{
    ++f.counter;
    if (a) {
        doAThing();
        return f;
    } else
        doAnotherThing();
}
f.counter = 0;