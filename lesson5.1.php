<?php
// 1) Написать программу, которая выводит простые числа, т.е. делящиеся без 
// остатка только на себя и на 1.
// 5.1.1 while
    $x = 1;
    while ($x <= 100) {
        $y = $x;
        while ($x % $y == 0 & $x % 1 == 0) {
            echo $x . "</br>";
            $y++;
        }
        $x++;
    }
// 5.1.2. do while	
	$x = 1;
    do {
        echo $x. "</br>";
    }
    while ($x++<100 & $x % $x == 0 & $x % 1 == 0);
// 5.1.3. for	
	for ($x = 1; $x <= 100; $x++) {
        if ($x % $x == 0) {
            echo $x . "</br>";
        } elseif ($x % 1 == 0) {
            echo $x . "</br>";
        }
    }
// 5.1.4. foreach
	$mass = array();
    for ($x = 1; $x <= 100; $x++) {
        if ($x % $x == 0) {
            $mass[] = $x;
        } elseif ($x % 1 == 0) {
            $mass[] = $x;
        }
    }
    foreach($mass as $v)
        echo $v.'</br>';

// 2) Сгенерируйте 100 раз новое число и выведите на экран количество четных чисел из этих 100.
// 5.2.1 while
$x = 1;
    while ($x <= 100) {
		while ($x % 2 == 0) {
			echo $x . "</br>";
			$x++;
		}
		$x++;
    }
// 5.2.2. do while		
	$x = 1;
    do {
        if($x % 2 == 0) {
            echo $x. "</br>";
        }
    }
    while ($x++<=100);
// 5.2.3. for	
	for ($x=1; $x<=100; $x++) {
    if ($x % 2 == 0) {
        echo $x. "</br>";
    }
}
// 5.2.4. foreach
	$mass = array();
    for ($x = 1; $x <= 100; $x++) {
        if ($x % 2 == 0) {
            $mass[] = $x;
        }
    }
    foreach($mass as $v)
        echo $v.'</br>';
		
//	3) Сгенерируйте 100 раз число от 1 до 5 и выведите на экран сколько раз сгенерировались эти числа (1, 2, 3, 4 и 5).
// 5.3.1 while		
	$res = [];
	$x = 0;
	$arr = ['one' => 0, 'two' => 0, 'three' => 0, 'fourth' => 0, 'five' => 0];
	while ($x <= 100) {
		$rand = rand(1, 5);
		if ($rand == 1) {
			$arr['one'] = $arr['one']++ . "</br>";
		}
		elseif ($rand == 2) {
			$arr['two'] = $arr['two']++ . "</br>";
		}
		elseif ($rand == 3) {
			$arr['three'] = $arr['three']++ . "</br>";
		}
		elseif ($rand == 4) {
			$arr['fourth'] = $arr['fourth']++ . "</br>";
		}
		elseif ($rand == 5) {
			$arr['five'] = $arr['five']++ . "</br>";
		}
		$res[] = $rand;
		$x++;
		"<pre>";
		print_r($arr);
		"</pre>";
	}		
// 5.3.2. do while	
	$res = [];
    $x = 0;
    $arr = ['one' => 0, 'two' => 0, 'three' => 0, 'fourth' => 0, 'five' => 0];
    do {
        {
            $rand = rand(1, 5);
            if ($rand == 1) {
                $arr['one'] = $arr['one']++;
            }
            elseif ($rand == 2) {
                $arr['two'] = $arr['two']++;
            }
            elseif ($rand == 3) {
                $arr['three'] = $arr['three']++;
            }
            elseif ($rand == 4) {
                $arr['fourth'] = $arr['fourth']++;
            }
            elseif ($rand == 5) {
                $arr['fives'] = $arr['five']++;
            }
            $res[] = $rand;
            $x++;
        }
    } while ($x <= 100);
    "<pre>";
    print_r($arr);
    "</pre>";
// 5.3.3. for	
	$res = [];
    $x = 0;
    $arr = ['one' => 0, 'two' => 0, 'three' => 0, 'fourth' => 0, 'five' => 0];
    for ($x = 0; $x <= 100; $x++) {
        $rand = rand(1, 5);
        if ($rand == 1) {
            $arr['one'] = $arr['one']++;
        } elseif ($rand == 2) {
            $arr['one'] = $arr['two']++;
        } elseif ($rand == 3) {
            $arr['one'] = $arr['three']++;
        } elseif ($rand == 4) {
            $arr['one'] = $arr['fourth']++;
        } elseif ($rand == 5) {
            $arr['one'] = $arr['five']++;
        }
    }
    $res[] = $rand;
    $x++;
    "<pre>";
    print_r($arr);
    "</pre>";
// 5.3.4. foreach	
	$randArray = [];
    for ($x = 0; $x <= 100; $x++) {
        $randArray[] = rand(1, 5);
   }
        $arr = ['one' => 0, 'two' => 0, 'three' => 0, 'fourth' => 0, 'five' => 0];
        foreach ($randArray as $key => $value) {
         if ($value == 1) {
            $arr['one']++;
        }
         elseif ($value == 2) {
            $arr['two']++;
        }
         elseif ($value == 3) {
            $arr['three']++;
        }
         elseif ($value == 4) {
            $arr['fourth']++;
        }
         elseif ($value == 5) {
            $arr['five']++;
        }
    }
    "<pre>";
    print_r($arr);
    "</pre>";

//4) Используя условия и циклы сделать таблицу в 5 колонок и 3 строки (5x3), отметить разными цветами часть ячеек.
// 5.4.1. while		
	$a = 0;
    echo "<table border='1'";
    while ($a < 150 && $a += 51) {
        echo "<tr>";
        $b = 0;
        while ($b < 5 && $b += 51) {
            $c = 0;
            while ($c < 220 && $c += 51) {
                echo "<td style=background-color:rgb($a,$b,$c);>&nbps</td>";
            }
        }
    }
    echo "</tr>";
    echo "</table>";
// 5.4.2. do while		
	$a = 0;
	do {
		echo "<table border='1'";
	}
	while ($a < 150 && $a += 51) {
		echo "<tr>";
		$b = 0;
		while ($b < 5 && $b += 51) {
			$c = 0;
			while ($c < 220 && $c += 51) {
				echo "<td style=background-color:rgb($a,$b,$c);>&nbps</td>";
			}
		}		
	}	
	echo "</tr>";
	echo "</table>";
// 5.4.3. for	
	echo "<table border='1'";
	for ($a = 0; $a < 150; $a += 51) {
		echo "<tr>";
		for ($b = 0; $b < 5; $b += 51) {
			for ($c = 0; $c < 220; $c += 51) {
				echo "<td style=background-color:rgb($a,$b,$c);>&nbps</td>";
			}
		}
	}
	echo "</tr>";
	echo "</table>";	
// 5.4.4. foreach	
	$mass = array();
    echo "<table border='1'";
	for ($a = 0; $a < 150; $a += 51) {
		echo "<tr>";
		for ($b = 0; $b < 5; $b += 51) {
			for ($c = 0; $c < 220; $c += 51) {
				echo "<td style=background-color:rgb($a,$b,$c);>&nbps</td>";
			}
		}
	}
    foreach($mass as $v)
        echo $v.'</br>';	
?>