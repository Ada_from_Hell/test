<?php
// 4. Найдите второе наибольшее число в массиве.

//4.1.
$arr1 = [1, 2, 3];

// 4.1.1. Function declaration
function max2Arr($arr1)
{
    sort($arr1);
    $key = count($arr1) - 2;
    return $arr1[$key];
}

echo max2Arr($arr1);;

// 4.1.2. Function expression
$max2Arr = function ($arr1) {
    sort($arr1);
    $key = count($arr1) - 2;
    return $arr1[$key];
};
echo $max2Arr ($arr1);

// 4.2
$arr2 = [1, -2, 3];

// 4.2.1. Function declaration
function max2Arr($arr2)
{
    sort($arr2);
    $key = count($arr2) - 2;
    return $arr2[$key];
}

echo max2Arr($arr2);;

// 4.2.2. Function expression
$max2Arr = function ($arr2) {
    sort($arr2);
    $key = count($arr2) - 2;
    return $arr2[$key];
};
echo $max2Arr ($arr2);

// 4.3
$arr3 = [0, 0, 10];

// 4.3.1. Function declaration
function max2Arr($arr3)
{
    sort($arr3);
    $key = count($arr3) - 2;
    return $arr3[$key];
}

echo max2Arr($arr3);;

// 4.3.2. Function expression
$max2Arr = function ($arr3) {
    sort($arr3);
    $key = count($arr3) - 2;
    return $arr3[$key];
};
echo $max2Arr ($arr3);

// 4.4
$arr4 = [-1, -2, -3];

// 4.4.1. Function declaration
function max2Arr($arr4)
{
    sort($arr4);
    $key = count($arr4) - 2;
    return $arr4[$key];
}

echo max2Arr($arr4);;

// 4.4.2. Function expression
$max2Arr = function ($arr4) {
    sort($arr3);
    $key = count($arr4) - 2;
    return $arr4[$key];
};
echo $max2Arr ($arr4);