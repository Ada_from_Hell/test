<?php
// 7. Создайте функцию которая принимает целое число и возвращает строку с названием фигуры,
// состоящий из переданного количество сторон.

// 7.1.
$i = 1;

// 7.1. Function declaration
function showFigure($i)
{
    if ($i == 1) {
        return 'Circle';
    } elseif ($i == 2) {
        return 'Semi-circle';
    } elseif ($i == 3) {
        return 'Triangle';
    } elseif ($i == 4) {
        return 'Square';
    } elseif ($i == 5) {
        return 'Pentagon';
    } elseif ($i == 6) {
        return 'Hexagon';
    } elseif ($i == 7) {
        return 'Heptagon';
    } elseif ($i == 8) {
        return 'Octagon';
    } elseif ($i == 9) {
        return 'Nonagon';
    } elseif ($i == 10) {
        return 'Decagon';
    }
}

echo showFigure($i);

// 7.2. Function expression
$showFigure = function ($i) {
    if ($i == 1) {
        return 'Circle';
    } elseif ($i == 2) {
        return 'Semi-circle';
    } elseif ($i == 3) {
        return 'Triangle';
    } elseif ($i == 4) {
        return 'Square';
    } elseif ($i == 5) {
        return 'Pentagon';
    } elseif ($i == 6) {
        return 'Hexagon';
    } elseif ($i == 7) {
        return 'Heptagon';
    } elseif ($i == 8) {
        return 'Octagon';
    } elseif ($i == 9) {
        return 'Nonagon';
    } elseif ($i == 10) {
        return 'Decagon';
    }
};
echo $showFigure($i);