<?php
// 8. Фермер просит вас посчитать сколько ног у всех его животных.
// Фермер разводит три вида: курицы = 2 ноги коровы = 4 ноги свиньи = 4 ноги.
// Фермер посчитал своих животных и говорит вам, сколько их каждого вида.
// Вы должны написать функцию, которая возвращает общее число ног всех животных.
$hen = 2;
$cow = 3;
$pig = 5;

// 8.1. Function declaration
   function calcPaws($hen, $cow, $pig) {
       return ($hen * 2) + ($cow * 4) ($pig * 4);
   }
echo calcPaws($hen, $cow, $pig);

// 8.2. Function expression
$calcPaws = function ($hen, $cow, $pig) {
    return ($hen * 2) + ($cow * 4) ($pig * 4);
};
echo $calcPaws($hen, $cow, $pig);