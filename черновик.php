<?php
//Создайте функцию, которая принимает слово на английском языке и проверяет, во множественном ли числе находится слово.
// Проверяйте самый простой вариант.
//Примеры:
//false  == solution("fork")
//true  == solution("forks")
//false  == solution("clojure")
//true  == solution("bytes")
//false  == solution("test")
$string = "bytes";

// 8.1. Function declaration
function checkPlural($string)
{
    if (substr($string, -1) == "s") {
        var_dump(boolval($string));
    } else {
        var_dump(boolval($string));
    }
}

echo checkPlural($string);

// 8.2. Function expression
$checkPlural = function ($string) {
    if (substr($string, -1) == "s") {
        var_dump(boolval($string));
    } else {
        var_dump(boolval($string));
    }
};

echo $checkPlural($string);