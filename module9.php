<?php
//Создайте функцию, которая трансформирует массив слов в массив длин этих слов.
//Примеры:
//[5,5]  == solution(["hello","world"])
//[4,4,4,7]  == solution(["some","test","data","strings"])
//[7]  == solution(["clojure"])

$arr1 = ["hello","world"];
echo iconv_strlen($arr1);