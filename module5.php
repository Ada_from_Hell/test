<?php
// 5. Дан массив строк, создайте функцию, которая создает новый массив, содержащий строки,
// длины которых соответствуют наидлиннейшей строке.

// 5.1.
$arr1 = ["in", "Soviet", "Russia", "frontend", "programms", "you"];

// 5.1.1. Function declaration
function createArrLongestString($arr1)
{
    $cachedLength = 0;
    $longest = [];
    foreach ($arr1 as $value) {
        $currentLength = strlen($value);
        if ($currentLength > $cachedLength) {
            $longest = [$value];
            $cachedLength = $currentLength;
        } elseif ($currentLength == $cachedLength) {
            $longest[] = $value;
        }
    }
    var_export($longest);
}

print_r(createArrLongestString($arr1));

// 5.1.2. Function expression
$createArrLongestString = function ($arr1) {
    $cachedLength = 0;
    $longest = [];
    foreach ($arr1 as $value) {
        $currentLength = strlen($value);
        if ($currentLength > $cachedLength) {
            $longest = [$value];
            $cachedLength = $currentLength;
        } elseif ($currentLength == $cachedLength) {
            $longest[] = $value;
        }
    }
    var_export($longest);
};

print_r($createArrLongestString($arr1));

// 5.2.1. Function declaration
function createArrLongestString($arr2)
{
    $cachedLength = 0;
    $longest = [];
    foreach ($arr2 as $value) {
        $currentLength = strlen($value);
        if ($currentLength > $cachedLength) {
            $longest = [$value];
            $cachedLength = $currentLength;
        } elseif ($currentLength == $cachedLength) {
            $longest[] = $value;
        }
    }
    var_export($longest);
}

print_r(createArrLongestString($arr2));

// 5.2.2. Function expression
$createArrLongestString = function ($arr2) {
    $cachedLength = 0;
    $longest = [];
    foreach ($arr2 as $value) {
        $currentLength = strlen($value);
        if ($currentLength > $cachedLength) {
            $longest = [$value];
            $cachedLength = $currentLength;
        } elseif ($currentLength == $cachedLength) {
            $longest[] = $value;
        }
    }
    var_export($longest);
};

print_r($createArrLongestString($arr2));

// 5.3
$arr3 = ["a","b","c","d"];

// 5.3.1. Function declaration
function createArrLongestString($arr3)
{
    $cachedLength = 0;
    $longest = [];
    foreach ($arr3 as $value) {
        $currentLength = strlen($value);
        if ($currentLength > $cachedLength) {
            $longest = [$value];
            $cachedLength = $currentLength;
        } elseif ($currentLength == $cachedLength) {
            $longest[] = $value;
        }
    }
    var_export($longest);
}

print_r(createArrLongestString($arr3));

// 5.3.2. Function expression
$createArrLongestString = function ($arr3) {
    $cachedLength = 0;
    $longest = [];
    foreach ($arr3 as $value) {
        $currentLength = strlen($value);
        if ($currentLength > $cachedLength) {
            $longest = [$value];
            $cachedLength = $currentLength;
        } elseif ($currentLength == $cachedLength) {
            $longest[] = $value;
        }
    }
    var_export($longest);
};

print_r($createArrLongestString($arr3));